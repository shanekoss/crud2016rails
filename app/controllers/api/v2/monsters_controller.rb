module Api::V2
  class MonstersController < ApplicationController
    before_action :set_monster, only: [:show, :update, :destroy]

    # POST /monsters
    def create
      @monster = Monster.new(monster_params)

      if @monster.save
        render json: @monster, status: :created, location: @monster
      else
        render json: @monster.errors, status: :unprocessable_entity
      end
    end


    # GET /v1/users
    def index
      @monsters = Monster.all
      render json: @monsters #, serializer: MonsterSerializer
    end
    def get
      @monsters = Monster.find(params[:id])
      render json @monster
    end
    # GET /monsters/1
    def show
      render json: @monster #, each_serializer: MonsterSerializer
    end

    # DELETE /monsters/1
    def destroy
      @monster.destroy
    end

    # PATCH/PUT /monsters/1
    def update
      if @monster.update_attributes(monster_params)
        #render json: @monster
      else
        #render json: @monster.errors, status: :unprocessable_entity
      end
    end

    private

    def monster_params
      params["data"]["attributes"].permit(:name, :level, :imageurl, :price)
    end
    def set_monster
      @monster = Monster.find(params[:id])
    end

  end
end
