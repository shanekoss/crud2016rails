Rails.application.routes.draw do
  namespace :api do
    namespace :v2 do
      resources :monsters
    end
  end
end
