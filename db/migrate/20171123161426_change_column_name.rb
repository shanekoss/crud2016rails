class ChangeColumnName < ActiveRecord::Migration[5.1]
  def change
    rename_column :monsters, :imageUrl, :image_url
  end
end
