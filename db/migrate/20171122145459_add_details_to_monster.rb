class AddDetailsToMonster < ActiveRecord::Migration[5.1]
  def change
    add_column :monsters, :name, :string
    add_column :monsters, :level, :integer
    add_column :monsters, :imageUrl, :string
    add_column :monsters, :price, :integer
  end
end
