class ChangeColumnNameAgain < ActiveRecord::Migration[5.1]

  def change
    rename_column :monsters, :image_url, :imageurl
  end
end
